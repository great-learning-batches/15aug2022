package com.greatlearning.util;

import java.util.Arrays;

public class ArrayUtils {
	public static void mergeSort(int[] array) {
		splitAndMerge(array, 0, array.length - 1);
	}
	private static void splitAndMerge(int[] array, int left, int right) {
		if (left < right) {
			int mid = (left + right) / 2;
			splitAndMerge(array, left, mid);
			splitAndMerge(array, mid+1, right);
			merge(array, left, mid, right);
		}
	}
	private static void merge(int[] array, int left, int mid , int right) {
		int n1 = mid - left + 1;	// size of the left array
		int n2 = right - mid;		// size of the right array
		int[] leftArray = new int[n1];
		int[] rightArray = new int[n2];
		for (int i = 0; i < n1; i++) {
			leftArray[i] = array[left + i];
		}
		for (int i = 0; i < n2; i++) {
			rightArray[i] = array[mid + 1 + i];
		}
		int i = 0, j = 0;
		int k = left;
		while (i < n1 && j < n2) {
			if (leftArray[i] < rightArray[j]) {
				array[k++] = leftArray[i++];
			} else {
				array[k++] = rightArray[j++];
			}
		}
		while (i < n1) {
			array[k++] = leftArray[i++];
		}
		while (j < n2) {
			array[k++] = rightArray[j++];
		}
	}
	public static void rotateLeft(int[] array, int count) {
		for (int i = 0; i < count; i++) {
			rotateLeftOnce(array);
//			System.out.println("after rotating "+(i+1)+":"+Arrays.toString(array));
		}
	}
	private static void rotateLeftOnce(int[] array) {
		int temp = array[0];
		for (int i = 0; i < array.length-1; i++) {
			array[i] = array[i+1];
		}
		array[array.length-1] = temp;
	}
	public static int binarySearch(int[] array, int key) {
		return binarySearch(array, 0, array.length - 1, key);
	}
	private static int binarySearch(int[] array, int left, int right, int key) {
		if (left > right) {
			return -1;
		}
		int mid = (left + right) / 2;
		if (array[mid] == key) {
			return mid;
		}
		if (array[mid] > key) {
			return binarySearch(array, left, mid-1, key);
		} else {
			return binarySearch(array, mid + 1, right, key);
		}
	}
	public static int modifiedSearch(int[] array, int key) {
		int pivot = findPivot(array);
		System.out.println("pivot is "+pivot);
		if (pivot == -1) {
			return -1;
		}
		if (array[pivot] == key) {
			return pivot;
		}
		if (key < array[0]) {
			return binarySearch(array, pivot+1, array.length-1, key);
		} else {
			return binarySearch(array, 0, pivot-1, key);
		}
	}
	public static int findPivot(int[] array) { // the array is assumed to be sorted and rotated
		return findPivot(array, 0, array.length-1);
	}
	private static int findPivot(int[] array, int left, int right) {
		if (left > right) {
			return -1;
		}
		int mid = (left + right) / 2;
		if (array[mid] > array[mid+1]) {
			return mid;
		}
		if (array[mid] < array[mid-1]) {
			return mid - 1;
		}
		if (array[mid] < array[0]) {
			return findPivot(array, left, mid - 1);
		} else {
			return findPivot(array, mid + 1, right);
		}
	}
}

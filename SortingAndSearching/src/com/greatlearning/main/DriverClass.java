package com.greatlearning.main;

import java.util.Arrays;
import java.util.Scanner;

import com.greatlearning.util.ArrayUtils;

public class DriverClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number of elements in array");
		int n = scanner.nextInt();
		System.out.println("Enter elements of the array");
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println("original array:"+Arrays.toString(array));
		ArrayUtils.mergeSort(array);
		System.out.println("sorted array:"+Arrays.toString(array));
		ArrayUtils.rotateLeft(array, array.length / 2);
		System.out.println("rotated array:"+Arrays.toString(array));
		System.out.println("Enter key to search -1 to exit");
		int key = scanner.nextInt();
		while (key != -1) {
			System.out.println("index of key "+key+" is "+ArrayUtils.modifiedSearch(array, key));
			System.out.println("Enter key to search -1 to exit");
			key = scanner.nextInt();
		}
		System.out.println("Good Bye");
	}
}

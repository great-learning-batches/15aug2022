package com.greatlearning.banking.main;

import java.util.Scanner;

import com.greatlearning.banking.model.CustomerAccount;
import com.greatlearning.banking.service.BankingService;
import com.greatlearning.banking.util.OTPGenerator;

public class DriverClass {

	private static Scanner scanner = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BankingService service = new BankingService();
		CustomerAccount account = checkCredentials(service);
		if (account == null) {
			return;
		}
		int choice = getMenuChoice();
		while (choice != 4) {
		switch (choice) {
		case 1: {
			System.out.println("Enter amount to deposit");
			double amt = scanner.nextDouble();
			service.deposit(account.getAccountNumber(), amt);
			System.out.println(account);
			break;
		}
		case 2: {
			System.out.println("Enter amount to withdraw");
			double amt = scanner.nextDouble();
			if (!service.withdraw(account.getAccountNumber(), amt)) {
				System.out.println("innsufficient balance");
			}
			System.out.println(account);
			break;
		}
		case 3: {
			String generatedOtp = OTPGenerator.nextOTP();
			System.out.println("Your OTP for transfer is "+generatedOtp);
			String otp = scanner.next();
			if (!generatedOtp.equals(otp)) {
				break;
			}
			System.out.println("Enter account number to transfer to ");
			String toAcno = scanner.next();
			System.out.println("Enter amount to be transferred");
			double amt = scanner.nextDouble();
			if (!service.transfer(account.getAccountNumber(), toAcno, amt)) {
				System.out.println("innsufficient balance");
			}
			System.out.println(account);
			break;
		}
		default:
			System.out.println("invalid choice");
		}
		choice = getMenuChoice();
		}
	}
	private static CustomerAccount checkCredentials(BankingService service) {
		System.out.println("Enter acno");
		String acno = scanner.nextLine();
		System.out.println("Enter password");
		String passwd = scanner.nextLine();
		return service.authenticate(acno, passwd);
	}
	private static int getMenuChoice() {
		System.out.println("1. Deposit");
		System.out.println("2. Withdraw");
		System.out.println("3. Transfer");
		System.out.println("4. Logout");
		System.out.println("Enter choice");
		return scanner.nextInt();
	}
}

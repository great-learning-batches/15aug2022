package com.greatlearning.banking.model;

public class CustomerAccount {
	private String accountNumber;
	private String password;
	private double balance;
	public String getAccountNumber() {
		return accountNumber;
	}
	public String getPassword() {
		return password;
	}
	public double getBalance() {
		return balance;
	}
	public CustomerAccount(String accountNumber, String password, double balance) {
		super();
		this.accountNumber = accountNumber;
		this.password = password;
		this.balance = balance;
	}
	public void deposit(double amt) {
		this.balance += amt;
	}
	public boolean withdraw(double amt) {
		if (amt >= balance) {
			return false;
		}
		this.balance -= amt;
		return true;
	}
	@Override
	public String toString() {
		return "CustomerAccount [accountNumber=" + accountNumber + ", balance=" + balance
				+ "]";
	}
}

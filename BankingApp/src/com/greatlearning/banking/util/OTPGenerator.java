package com.greatlearning.banking.util;

import java.util.Random;

public class OTPGenerator {
	public static String nextOTP() { // OTPGenerator.nextOTP()
		Random r = new Random(); // use _ as grouping character
		return String.format("%4d", r.nextInt(10_000));
	}
}

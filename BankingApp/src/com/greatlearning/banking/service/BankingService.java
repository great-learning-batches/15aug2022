package com.greatlearning.banking.service;

import com.greatlearning.banking.model.CustomerAccount;

public class BankingService {
	private CustomerAccount[] accounts = new CustomerAccount[3];
	{
		accounts[0] = new CustomerAccount("254562", "secret0", 1_00_000.0);
		accounts[1] = new CustomerAccount("223452", "secret1", 1_00_000);
		accounts[2] = new CustomerAccount("564452", "secret2", 1_00_000);
	}
	public CustomerAccount authenticate(String acno, String passwd) {
		for (CustomerAccount account : accounts) {
			if (account.getAccountNumber().equals(acno) &&
					account.getPassword().equals(passwd)) {
				return account;
			}
		}
		return null;
	}
	private CustomerAccount getAccount(String acno) {
		for (CustomerAccount account : accounts) {
			if (account.getAccountNumber().equals(acno)) {
				return account;
			}
		}
		return null;
	}
	public void deposit(String acno, double amt) {
		getAccount(acno).deposit(amt);
	}
	public boolean withdraw(String acn, double amt) {
		return getAccount(acn).withdraw(amt);
	}
	public boolean transfer(String fromAcno, String toAcno, double amt) {
		if (! getAccount(fromAcno).withdraw(amt)) {
			return false;
		}
		getAccount(toAcno).deposit(amt);
		return true;
	}
}

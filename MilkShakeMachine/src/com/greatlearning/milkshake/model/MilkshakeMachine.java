package com.greatlearning.milkshake.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class MilkshakeMachine {
	private String[] milkshakeTypes;
	private int maxParallel;
	private int secondsPerGlass;
	private Queue<Integer> queue = new PriorityQueue<>(Comparator.reverseOrder());
	public MilkshakeMachine(String[] milkshakeTypes, int maxParallel, int secondsPerGlass) {
		super();
		this.milkshakeTypes = milkshakeTypes;
		this.maxParallel = maxParallel;
		this.secondsPerGlass = secondsPerGlass;
	}
	public String[] getMilkshakeTypes() {
		return milkshakeTypes;
	}
	public int getMaxParallel() {
		return maxParallel;
	}
	public int getSecondsPerGlass() {
		return secondsPerGlass;
	}
	public int processOrder(int[] quantities) {
		int timeInSeconds = 0;
		for (int qty : quantities) {
			this.queue.add(qty);
		}
		
		while (this.queue.peek() > 0) {
			process();
			timeInSeconds += secondsPerGlass;
		}
		return timeInSeconds;
	}
	private void process() {
		List<Integer> topQuantities = new ArrayList<>();
		int maxParallel = Math.min(this.maxParallel, this.milkshakeTypes.length);
		for (int i = 0; i < maxParallel; i++) {
			Integer qty = queue.peek();
			if (qty == 0) {
				break;
			}
			this.queue.remove();
			topQuantities.add(qty);
		}
		for (int qty : topQuantities)  {
			this.queue.add(--qty);
		}
	}
	public static void main(String[] args) {
		String[] milkshakeTypes = {"Mango", "Orange", "Pineapple"};
		int maxParallel = 2;
		int secondsPerGlass = 1;
		MilkshakeMachine machine = new MilkshakeMachine(milkshakeTypes, 
				maxParallel, secondsPerGlass);
		int[] orderQuantities = getOrder(machine);
		int timeInSeconds = machine.processOrder(orderQuantities);
		System.out.println("Minimum time needed to deliver all orders is"
				+ timeInSeconds);
	}
	private static int[] getOrder(MilkshakeMachine machine) {
		String[] milkshakeTypes = machine.getMilkshakeTypes();
		try (Scanner scanner = new Scanner(System.in);) {
			int[] quantities = new int[milkshakeTypes.length];
			int index = 0;
			for (String milkshakeType : milkshakeTypes) {
				System.out.println("Total number of orders for "
						+milkshakeType+" milkshake");
				quantities[index++] = scanner.nextInt();
			}
			return quantities;
		}
	}
}

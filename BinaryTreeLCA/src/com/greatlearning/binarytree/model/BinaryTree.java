package com.greatlearning.binarytree.model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntConsumer;

public class BinaryTree {
	public static class Node {
		private int data;
		private Node left, right;
		public Node(int data) {
			this.data = data;
		}
		public int data() {
			return this.data;
		}
		public Node left() {
			return this.left;
		}
		public Node right() {
			return this.right;
		}
		public Node left(int data) {
			return this.left = new Node(data);
		}
		public Node right(int data) {
			return this.right = new Node(data);
		}
		public String toString() {
			return ""+this.data;
		}
	}
	private Node root;
	public BinaryTree(int rootData) {
		this.root = new Node(rootData);
	}
	public Node getRoot() {
		return this.root;
	}
	public void traversePreOrder(IntConsumer action) {
		traversePreOrder(this.root, action);
	}
	public void traverseInOrder(IntConsumer action) {
		traverseInOrder(this.root, action);
	}
	public void traversePostOrder(IntConsumer action) {
		traversePostOrder(this.root, action);
	}
	private void traversePreOrder(Node node, IntConsumer action) {
		action.accept(node.data());
		if (node.left() != null) traversePreOrder(node.left(), action);
		if (node.right() != null) traversePreOrder(node.right(), action);
	}
	private void traverseInOrder(Node node, IntConsumer action) {
		if (node.left() != null) traverseInOrder(node.left(), action);
		action.accept(node.data());
		if (node.right() != null) traverseInOrder(node.right(), action);
	}
	private void traversePostOrder(Node node, IntConsumer action) {
		if (node.left() != null) traversePostOrder(node.left(), action);
		if (node.right() != null) traversePostOrder(node.right(), action);
		action.accept(node.data());
	}
	public List<Integer> findPath(int value) {
		List<Integer> path = new ArrayList<>();
		findPath(this.root, value, path); 
		return path;
	}
	private boolean findPath(Node node, int value, List<Integer> path) {
		if (node == null) return false;
		path.add(node.data());
		if (node.data() == value) return true;
		if (node.left() != null && findPath(node.left(), value, path)) return true;
		if (node.right() != null && findPath(node.right(), value, path)) return true;
		path.remove(path.size() - 1);
		return false;
	}
	public int findLCA(int value1, int value2) {
		List<Integer> path1 = this.findPath(value1);
		List<Integer> path2 = this.findPath(value2);
		if (path1.isEmpty() || path2.isEmpty()) {
			System.out.println(path1.isEmpty() ?  
					"node for " + value1 + " is missing" :
						"node for " + value1 + " is present");
			System.out.println(path2.isEmpty() ?  
					"node for " + value2 + " is missing" :
						"node for " + value2 + " is present");
			return -1;
		}
		int i = -1;
		for (i = 0; i < Math.min(path1.size(), path2.size()); i++) {
			if (path1.get(i) != path2.get(i)) {
				return path1.get(i-1);
			}
		}
		return path1.get(i-1);
	}
	public static void main(String[] args) {
		BinaryTree bTree = new BinaryTree(10);
		Node node20 = bTree.getRoot().left(20);
		Node node30 = bTree.getRoot().right(30);
		node20.left(40);
		node20.right(50);
		node30.left(60);
		node30.right(70);
		bTree.traversePreOrder(System.out::print);
//      bTree.traversePreOrder( a -> System.out.print(a));
/*
        bTree.traversePreOrder( new IntConsumer() {
                public void accept(int a) {
                    System.out.print(a);
                }
            });
        public class PrintInteger implements IntConsumer {
            public void accept(int a) {
                    System.out.print(a);
            }
        }
        bTree.traversePreOrder(new PrintInteger());
*/
		System.out.println("path to 10 "+bTree.findPath(10));
		System.out.println("path to 20 "+bTree.findPath(20));
		System.out.println("path to 30 "+bTree.findPath(30));
		System.out.println("path to 40 "+bTree.findPath(40));
		System.out.println("path to 50 "+bTree.findPath(50));
		System.out.println("path to 60 "+bTree.findPath(60));
		System.out.println("path to 70 "+bTree.findPath(70));
		System.out.println("path to 80 "+bTree.findPath(80));
		System.out.println("least Common Ancestor(20, 30):"+bTree.findLCA(20, 30));
		System.out.println("least Common Ancestor(40, 30):"+bTree.findLCA(40, 30));
		System.out.println("least Common Ancestor(60, 70):"+bTree.findLCA(60, 70));
		System.out.println("least Common Ancestor(20, 40):"+bTree.findLCA(20, 40));
	}
}